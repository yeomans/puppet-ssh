class ssh::config::sshd
    (
    $passwordAuthentication='yes',
    $permitRootLogin='yes',
    )
    {
    file
        {
        '/etc/ssh/sshd_config':
        ensure => present, 
        owner => 'root', 
        group => 'root', 
        mode => '0644', 
        content => template("ssh/sshd_config.erb")
        }
    }
    
    
    
    
class ssh::config
    {
    contain ssh::config::sshd
    }